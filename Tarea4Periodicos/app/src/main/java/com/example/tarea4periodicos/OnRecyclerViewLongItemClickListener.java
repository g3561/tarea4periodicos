package com.example.tarea4periodicos;

import android.view.View;

public interface OnRecyclerViewLongItemClickListener {
    public void OnItemLongClick(View view, int posicion);
}
