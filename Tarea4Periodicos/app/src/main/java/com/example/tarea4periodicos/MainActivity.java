package com.example.tarea4periodicos;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private List<Periodico> periodicos;
    private int posicionPeriodico;
    private AdaptadorPeriodicos adaptadorPeriodicos;
    private DBInterface dbInterface;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        periodicos = new ArrayList<Periodico>();

        dbInterface = new DBInterface(this); //Instancia de la base de datos
        dbInterface.abre(); //Comprueba si existe, si existe la abre, si no, la crea. Devuelve la instancia

        cargarPeriodicos();

        RecyclerView recyclerPeriodicos = findViewById(R.id.recyclerPeriodicos);
        recyclerPeriodicos.setLayoutManager(new LinearLayoutManager(this));

        adaptadorPeriodicos = new AdaptadorPeriodicos(periodicos);

        recyclerPeriodicos.setAdapter(adaptadorPeriodicos);

        registerForContextMenu(recyclerPeriodicos);

        adaptadorPeriodicos.setOnItemLongClickListener(new OnRecyclerViewLongItemClickListener() {
            @Override
            public void OnItemLongClick(View view, int posicion) {
                posicionPeriodico = posicion;
                openContextMenu(view); //Se abre el menú contextual cuando se hace una pulsación larga
            }
        });
    }

    /**
     * Se llamará cuando se cierre la actividad
     */
    @Override
    public void onStop() {
        super.onStop();
        dbInterface.cierra();
    }

    /**
     * Se llamará cuando se accede a otra actividad y posteriormente esta vuelve a ejecutarse
     */
    @Override
    public void onResume() {
        super.onResume();
        dbInterface.abre(); //Se vuelve a abrir la instancia a la bd al volver a la actividad
        cargarPeriodicos(); //Se cargan los títulos
        adaptadorPeriodicos.notifyDataSetChanged(); //Se avisa al adaptador de que han habido cambios en la bd
    }

    /**
     * Método que consulta los periódicos de la base de datos y se los pasa a la lista
     */
    public void cargarPeriodicos() {
        periodicos.clear();
        Cursor c = dbInterface.obtenerPeriodicos();
        if (c == null) {
            Toast.makeText(this, "Tabla vacía", Toast.LENGTH_LONG).show();
        } else {
            if (c.moveToFirst()) {
                do {
                    periodicos.add(new Periodico(c.getLong(0), c.getString(1), c.getString(2))); //Se coge la segunda y tercera columna de la tabla (título y subtítulo)
                } while (c.moveToNext()); //Mientras se pueda acceder al siguiente, va avanzando el cursor
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_toolbar, menu);
        return true;
    }


    public void onAnadirMedioSelected(MenuItem item) {
        startActivity(new Intent(this, ActividadInsertar.class));
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        Activity activity = (Activity) v.getContext();
        MenuInflater inflater = activity.getMenuInflater();
        inflater.inflate(R.menu.recyclerview_context, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        System.out.println("Entra al método onContextItemSelected");
        switch (item.getItemId()) {
            case R.id.borrar:
                if (dbInterface.borrarPeriodico(periodicos.get(posicionPeriodico).getId()) == 1) {
                    periodicos.remove(posicionPeriodico); //Se elimina el titular del ArrayList
                    adaptadorPeriodicos.notifyDataSetChanged(); //Refresca el recycler
                    Toast.makeText(this, "Periódico eliminado",
                            Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(this, "Error al borrar el periódico",
                            Toast.LENGTH_LONG).show();
                }
                break;
        }
        return true;
    }
}