package com.example.tarea4periodicos;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class ActividadInsertar extends AppCompatActivity {

    private EditText nombre;
    private RadioGroup group;
    private DBInterface dbInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_insertar);

        nombre = findViewById(R.id.nombreEditText);
        group = findViewById(R.id.radioGroup);

        dbInterface = new DBInterface(this);
    }

    public void onClickInsertar(View v) {
        if (nombre.getText().toString().equals("")) {
            Toast.makeText(this, "Debe especificar un nombre para el periódico", Toast.LENGTH_LONG).show();
        } else {
            int radioButtonId =  group.getCheckedRadioButtonId();
            if (radioButtonId == -1) {
                Toast.makeText(this, "Debe seleccionar una temática para el nuevo periódico", Toast.LENGTH_LONG).show();
            } else {
                RadioButton radioButton = findViewById(radioButtonId);
                dbInterface.abre();
                if (dbInterface.insertarPeriodico(nombre.getText().toString(), radioButton.getText().toString()) == -1) {
                    Toast.makeText(this, "Error de inserción", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "Periódico insertado", Toast.LENGTH_LONG).show();
                    finish(); //Se cierra la actividad
                }
            }

        }
    }
}