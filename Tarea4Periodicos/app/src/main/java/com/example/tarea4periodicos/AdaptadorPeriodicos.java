package com.example.tarea4periodicos;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class AdaptadorPeriodicos extends RecyclerView.Adapter<AdaptadorPeriodicos.ViewHolder> {

    private List<Periodico> datos;

    OnRecyclerViewLongItemClickListener itemLongClickListener;

    /**
     * Método que registra el escuchador del evento
     * @param listener
     */
    public void setOnItemLongClickListener(OnRecyclerViewLongItemClickListener listener) {
        this.itemLongClickListener = listener;
    }

    public AdaptadorPeriodicos(List<Periodico> datos) {
        this.datos = datos;
    }

    /**
     * Método que se llamará por cada elemento del layout
     * @param parent
     * @param viewType
     * @return
     */
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //Se obtiene el layout
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.linea_recycler, parent, false);

        //Se instancia el ViewHolder pasándole el view obtenido. El método se llamará tantas veces
        //como elementos tenga el layout
        ViewHolder viewHolder = new ViewHolder(v);

        //Se devuelve el viewHolder
        return viewHolder;
    }

    /**
     * Método que se utiliza para introducir los datos
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.nombre.setText(datos.get(position).getNombre());
        holder.tematica.setText(datos.get(position).getTematica());
    }

    /**
     * Método que se utiliza para saber el número de datos de la lista
     * @return
     */
    @Override
    public int getItemCount() {
        return datos.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener {

        private TextView nombre, tematica;

        /*El itemView pasado como parámetro es el layout creado por nosotros (linea_recycler)
         * Lo que se hace es que cada vez que se instancie el layout, se obtienen las referencias
         * de los elementos del layout título y subtítulo de forma dinámica, algo que no se podría
         * hacer sin esto*/
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            nombre = itemView.findViewById(R.id.nombrePeriodico);
            tematica = itemView.findViewById(R.id.tematicaPeriodico);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public boolean onLongClick(View view) {
            if (itemLongClickListener != null) {
                itemLongClickListener.OnItemLongClick(view, getAdapterPosition());
            }
            return true;
        }
    }
}
